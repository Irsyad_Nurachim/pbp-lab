from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    Notes = Note.objects.all().values()
    response = {'Notes': Notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm()

    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/lab-4')


    context = {'form':form}
    return render(request, 'lab4_form.html', context)

def note_list(request):
    Notes = Note.objects.all().values()
    response = {'Notes': Notes}
    return render(request, 'lab4_note_list.html', response)