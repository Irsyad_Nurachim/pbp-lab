from django.db import models

# Create your models here.
class Note(models.Model):

    toReceiver = models.CharField(max_length=30)
    fromSender = models.CharField(max_length=30)
    judul = models.CharField(max_length=30)
    messagesSender = models.CharField(max_length=30)