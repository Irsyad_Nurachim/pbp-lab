from django.urls import path
from .views import index, jsonMethod, xmlMethod 

urlpatterns = [
    path('', index , name = 'index'),
    path('xml', xmlMethod, name = 'xmlMethod'),
    path('json', jsonMethod, name = 'jsonMethod')
]