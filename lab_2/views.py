from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note

# Create your views here.
def index(request):
    Notes = Note.objects.all().values()
    response = {'Notes': Notes}
    return render(request, 'lab2.html', response)

def xmlMethod(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def jsonMethod(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
