from django.urls import path
from .views import add_friends, index

urlpatterns = [
    path('', index, name='index'),
    path('add', add_friends, name='add_friends')

]